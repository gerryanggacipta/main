package org;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.util.GlobalConfig;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        GlobalConfig.getInstance().setPrimaryStage(primaryStage);

        Parent root = FXMLLoader.load(getClass().getResource("/login.fxml"));
        primaryStage.setTitle("Carpark Analytics");
        primaryStage.setScene(new Scene(root, 790, 490));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
