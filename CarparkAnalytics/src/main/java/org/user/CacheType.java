package org.user;

public enum CacheType {
    WATCHLIST, ORDERS, PREFERENCES
}
