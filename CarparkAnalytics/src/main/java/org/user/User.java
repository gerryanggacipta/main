package org.user;

import java.util.concurrent.ConcurrentHashMap;

public abstract class User {

    protected String userID;
    protected ConcurrentHashMap<CacheType, Object> userCache;

    public String getUserID() {
        return this.userID;
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof User)) {
            return false;
        }

        String secondUserID = ((User) o).getUserID();

        if (this.getUserID().equalsIgnoreCase(secondUserID)) {
            return true;
        }

        return false;
    }

    abstract void login();

}
