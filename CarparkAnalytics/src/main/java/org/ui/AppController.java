package org.ui;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.util.GlobalConfig;

public class AppController {

    @FXML
    private TextField username;

    @FXML
    private void userClickLoginButton(Event event) {

        if (event instanceof KeyEvent && !KeyCode.ENTER.equals(((KeyEvent) event).getCode())) {
            return;
        }


        try {

            Parent root = FXMLLoader.load(getClass().getResource("../../../resources/app.fxml"));

            Stage primaryStage = GlobalConfig.getInstance().getPrimaryStage();
            primaryStage.close();

            primaryStage.setScene(new Scene(root, 900, 600));
            primaryStage.setResizable(false);
            primaryStage.show();

        } catch (Exception e) {

        }

    }

}
