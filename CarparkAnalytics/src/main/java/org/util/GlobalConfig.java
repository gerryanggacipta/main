package org.util;

import javafx.stage.Stage;

public class GlobalConfig {

    private volatile Stage primaryStage;

    private GlobalConfig() {
    }

    private static volatile GlobalConfig instance;

    public static GlobalConfig getInstance() {

        if (instance == null) {
            synchronized (GlobalConfig.class) {
                if (instance == null) {
                    instance = new GlobalConfig();
                }
            }
        }

        return instance;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public synchronized void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

}
